<?php

use Illuminate\Support\Facades\Route;


Route::get('/', array('uses' => 'Front\HomePageController@homePage'));

Route::get('/login', array('uses' => 'Users\UserLoginController@UserLoginPage'));
Route::post('/login', array('as' => 'UserLoginProcess', 'uses' => 'Users\UserLoginController@UserLoginProcess', 'before' => 'csrf'));
Route::any('/logout', 'Users\UserLoginController@logout');

Route::get('/dashboard', array('as' => 'getUserList', 'middleware' => 'auth', 'uses' => 'Dashboard\DashboardController@getUserList'));
Route::get('/spielerdetails/{losnummer}', array('as' => 'getUserList', 'middleware' => 'auth', 'uses' => 'Dashboard\DashboardController@getUserDetails'));
Route::post('/ajax_spielerdata_save', array('middleware' => 'auth', 'uses' => 'Dashboard\DashboardController@setUserDetails', 'before' => 'csrf'));


Route::get('/meinprofil', array('as' => 'getUserList', 'middleware' => 'auth', 'uses' => 'Dashboard\ProfilController@getMeinProfil'));
Route::post('/ajax_profil_andern', array('middleware' => 'auth', 'uses' => 'Dashboard\ProfilController@setProfil', 'before' => 'csrf'));
Route::post('/ajax_passwort_andern', array('middleware' => 'auth', 'uses' => 'Dashboard\ProfilController@setPasswort', 'before' => 'csrf'));


Route::post('/ajax_spieler_save', array('uses' => 'Front\SpielerController@spielSave', 'before' => 'csrf'));


Route::get('auth/facebook', 'Auth\LoginController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\LoginController@handleFacebookCallback');


