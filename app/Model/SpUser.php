<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SpUser extends Model
{
    protected $table = 'sp_spieler';
    protected $fillable = [
        'cookies',
        'losnummer',
        'status',
        'anrede',
        'vorname',
        'nachname',
        'geb_datum',
        'strasse',
        'plz',
        'ort',
        'handy',
        'festnetz',
        'email',
        'vodafone_kunde',
        'vertrag_art',
        'aktuelle_vertrag',
        'vertrag_name',
        'vertrag_anbieter',
        'vertrag_ende',
        'kaufe_zubehor',
        'information_produkt',
        'information_produkt_name',
        'information_preis',
        'information_preis_name',
        'information_angebot',
        'information_angebot_name',
        'newsletter',
        'notizen',
        'created_at',
        'updated_at'
    ];
}
