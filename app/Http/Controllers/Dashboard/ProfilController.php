<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use DB;
use Hash;
use Illuminate\Support\Facades\Auth;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function getMeinProfil()
    {
        $meinProfil = DB::table('users')
            ->select(['vorname', 'nachname'])
            ->get()->toArray();
        return view('admin.page.meinprofil', compact('meinProfil'));
    }

    public function setProfil(Request $request)
    {
        DB::table('users')
            ->where('id', session('id'))
            ->update(['nachname' => $request->input('nachname'), "vorname" => $request->input('vorname')]);
        return response()->json(true);
    }

    public function setPasswort(Request $request)
    {
        if ($request->input('password_old') == Auth::user()->password_normal) {
            $update_password = array('password' => Hash::make($request->input('password')), 'password_normal' => $request->input('password'));
            DB::table('users')
                ->where('id', session('id'))
                ->update($update_password);

            return response()->json(true);
        } else {
            return response()->json(false);
        }
    }
}
