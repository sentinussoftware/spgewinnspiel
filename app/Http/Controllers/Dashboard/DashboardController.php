<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Session;
use DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function getUserList()
    {

        $userList_db = DB::table('sp_spieler')
            ->get();

        $userList = array();
        foreach ($userList_db as $values) {

            $userList[] = array(
                "id" => $values->id,
                "losnummer" => $values->losnummer,
                "status" => $values->status,
                "name" => $values->nachname . ", " . $values->vorname,
                "geb_datum" => $values->geb_datum,
                "adresse" => $values->strasse . " " . $values->plz . " " . $values->ort,
                "email" => $values->email,
                "vodafone_kunde" => $values->vodafone_kunde == 1 ? "JA" : "NEIN",
                "created_at" => $values->created_at,
            );
        }
        return response(view('admin.page.dashboard', array("data" => $userList)), 200);
    }

    public function getUserDetails($losnummer)
    {
        $spielerDetails = DB::table('sp_spieler')
            ->where('losnummer', $losnummer)
            ->get()->toArray();

        return view('admin.page.spielerdetails', compact('spielerDetails'));
    }

    public function setUserDetails(Request $request)
    {
        DB::table('sp_spieler')
            ->where('losnummer', $request->losnummer)
            ->update([
                'vorname' => $request->vorname,
                'nachname' => $request->nachname,
                'strasse' => $request->strasse,
                'plz' => $request->plz,
                'ort' => $request->ort,
                'handy' => $request->handy,
                'festnetz' => $request->festnetz,
                'email' => $request->email,
                'notizen' => $request->notizen
            ]);
        return response()->json(true);
    }
}
