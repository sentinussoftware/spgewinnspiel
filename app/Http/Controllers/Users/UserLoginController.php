<?php

namespace App\Http\Controllers\Users;

use Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use Webpatser\Uuid\Uuid;

class UserLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function UserLoginPage()
    {
        return view('admin.page.login_page');
    }

    public function UserLoginProcess(Request $request)
    {

        $postData = Request::all();

        $rules = array(
            'email' => 'required|email',
            'password' => 'required'
        );

        $messages = array(
            'email.required' => 'E-Mail adresi gerekli.',
            'email.email' => 'Lütfen geçerli bir mail adresi yazın',
            'password.required' => 'Lütfen şifrenizi yazın'
        );

        $validator = \Validator::make($postData, $rules, $messages);

        if ($validator->fails()) {

            return Redirect::to('/login')
                ->withErrors($validator);
        } else {
            if (Auth::attempt(array('email' => $postData['email'], 'password' => $postData['password']))) {
                Session::put('id', Auth::user()->id);
                return Redirect::to('/dashboard');
            } else {
                return Redirect::to('/login')
                    ->withErrors(array('no' => 'Überprüfen Sie Ihre Angaben und versuchen Sie es erneut oder wenden Sie Ihre Super an.'));
            }
        }
    }

    public function logout()
    {
        \Auth::logout();
        \Session::flush();
        \Cache::flush();
        return Redirect::to('/');
    }
}
