<?php

namespace App\Http\Controllers\Front;


use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Webpatser\Uuid\Uuid;

class SpielerController extends Controller
{
    public function spielSave(Request $request)
    {

        $postAll = $request->all();

        /* $find_user = \DB::table('sp_spieler')
             ->where('email', $postAll['email'])
             ->where('cookies', $postAll['cookies'])
             ->count();

         if ($find_user > 0) {
        */

        //Losnummer
        Session::put('losnummer', $postAll['losnummer']);
        Session::put('status', $postAll['status']);

        $spuser = new \App\Model\SpUser();
        $spuser->cookies = session('cookies');
        $spuser->losnummer = session('losnummer');
        $spuser->status = isset($postAll['status']) ? $postAll['status'] : 'Kein Gewinn';
        $spuser->anrede = 0;
        $spuser->vorname = $postAll['vorname'];
        $spuser->nachname = $postAll['nachname'];
        $spuser->geb_datum = "";
        $spuser->strasse = "";
        $spuser->plz = "";
        $spuser->ort = "";
        $spuser->handy = $postAll['handy'];
        $spuser->festnetz = "";
        $spuser->email = $postAll['email'];
        $spuser->vodafone_kunde = isset($postAll['vodafone_kunde']) ? $postAll['vodafone_kunde'] : '0';
        $spuser->vertrag_art = isset($postAll['vertrag_art']) ? $postAll['vertrag_art'] : '';
        $spuser->aktuelle_vertrag = isset($postAll['aktuelle_vertrag']) ? $postAll['aktuelle_vertrag'] : '0';
        $spuser->vertrag_name = isset($postAll['vertrag_name']) ? $postAll['vertrag_name'] : '';
        $spuser->vertrag_anbieter = isset($postAll['vertrag_anbieter']) ? $postAll['vertrag_anbieter'] : '';
        $spuser->vertrag_ende = isset($postAll['vertrag_ende']) ? $postAll['vertrag_ende'] : '';
        $spuser->kaufe_zubehor = isset($postAll['kaufe_zubehor']) ? $postAll['kaufe_zubehor'] : '0';
        $spuser->information_produkt = isset($postAll['information_produkt']) ? $postAll['information_produkt'] : '0';
        $spuser->information_produkt_name = isset($postAll['information_produkt_name']) ? $postAll['information_produkt_name'] : '';
        $spuser->information_preis_name = isset($postAll['information_preis_name']) ? $postAll['information_preis_name'] : '';
        $spuser->information_angebot = isset($postAll['information_angebot']) ? $postAll['information_angebot'] : '0';
        $spuser->information_angebot_name = isset($postAll['information_angebot_name']) ? $postAll['information_angebot_name'] : '';
        $spuser->newsletter = isset($postAll['newsletter']) ? $postAll['newsletter'] : '0';
        $spuser->created_at = Carbon::now()->timezone('Europe/Berlin');
        $spuser->updated_at = Carbon::now()->timezone('Europe/Berlin');
        $spuser->save();
        /*   }  */


        if($postAll['status'] !="Kein Gewinn") {
        $mail_data = array(
            'vorname' => $postAll['vorname'],
            'nachname' => $postAll['vorname'],
            'losnummer' => session('losnummer')
        );
        $toEmail = $postAll['email'];
        $toName = $postAll['email'];

        \Mail::send("mail.gewinn_mail", $mail_data, function ($message)
        use ($toEmail, $toName) {

            $message->to($toEmail, '')
                ->from('noreply@sp-gewinnspiel.de', 'SP-Service Plattform GmbH')
                ->subject("Dein Gewinn wartet auf dich!");
        });
    }
          
        

        return response()->json(array('losnummer' => session('losnummer'), 'status' => session('status')));
    }
}