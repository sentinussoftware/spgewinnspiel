<?php

namespace App\Http\Controllers\Front;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class HomePageController extends Controller
{

    public function homePage(Request $request)
    {
        $find_user_db = \DB::table('sp_spieler')
            ->where('cookies', $request->session()->get('_token'))
            ->get()->toArray();

        Session::put('cookies', $request->session()->get('_token'));

        if (count($find_user_db) > 0) {
            Session::put('losnummer', $find_user_db[0]->losnummer);
            Session::put('status', $find_user_db[0]->status);
        } else {
            Session::put('losnummer', '');
            Session::put('status', '');
        }
        
        return view('front.home');
    }
}
