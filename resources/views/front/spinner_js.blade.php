<script>

    let formdata;

    let canvas = new Winwheel({
        'canvasId': 'canvas',
        'rotationAngle': 0,
        'numSegments': 10,
        'centerX': 350,
        'centerY': 350,
        'drawMode': 'image',
        'rotationAngle': 0,
        'pointerAngle': 90,
        'responsive': false,
        'scaleFactor': 0.85,
        'segments':
            [
                {'id': '1', 'text': 'Powerbank'},
                {'id': '2', 'text': 'Iphone 11'},
                {'id': '3', 'text': '25 Euro Gutschein'},
                {'id': '4', 'text': 'Xiamio MI 10 pro'},
                {'id': '5', 'text': 'Vielleicht beim \nnächstenmal!'},
                {'id': '6', 'text': 'Powerbank'},
                {'id': '7', 'text': '25 Euro Gutschein'},
                {'id': '8', 'text': 'Iphone 11'},
                {'id': '9', 'text': 'Vielleicht beim \nnächstenmal!'},
                {'id': '10', 'text': 'Xiamio MI 10 pro'},
            ],
        'animation':
            {
                'type': 'spinToStop',
                'duration': 5,
                'spins': 8,
                'callbackFinished': alertPrize
            }
    });

    /*
        // Create image in memory.
        let handImage = new Image();
        handImage.onload = function () {
            let handCanvas = document.getElementById('canvas');
            let ctxs = handCanvas.getContext('2d');
            if (ctxs) {
                ctxs.save();
                ctxs.translate(100, 150);
                ctxs.rotate(canvas.degToRad(-40));
                ctxs.translate(230, 30);
                ctxs.drawImage(handImage, 255, 110);
                ctxs.restore();
            }
        };
        handImage.src = '{{ asset('images/cursor.png') }}';
*/

    let wheelPower = 0;
    let wheelSpinning = false;

    // -------------------------------------------------------
    // Click handler for spin button.
    // -------------------------------------------------------
    function startSpin() {
        if (wheelSpinning == false) {
            if (wheelPower == 1) {
                canvas.animation.spins = 3;
            } else if (wheelPower == 2) {
                canvas.animation.spins = 8;
            } else if (wheelPower == 3) {
                canvas.animation.spins = 15;
            }

            canvas.startAnimation();
            wheelSpinning = true;
        }
    }

    // -------------------------------------------------------
    // Function for reset button.
    // -------------------------------------------------------
    function resetWheel() {
        canvas.stopAnimation(false);
        canvas.rotationAngle = 0;
        canvas.draw();
        wheelSpinning = false;
    }

    // Create new image object in memory.
    let loadedImg = new Image();

    // Create callback to execute once the image has finished loading.
    loadedImg.onload = function () {
        canvas.wheelImage = loadedImg;
        canvas.draw();
    }

    // Set the image source, once complete this will trigger the onLoad callback (above).
    loadedImg.src = '{{ asset('images/glucksrad.png') }}';


    function alertPrize(indicatedSegment) {
        if (indicatedSegment.id == "5" || indicatedSegment.id == "9") {
            formdata.append('status', 'Kein Gewinn');
            $.ajax({
                url: '/ajax_spieler_save',
                type: 'POST',
                data: formdata,
                processData: false,
                contentType: false,
                beforeSend: function () {
                },
                success: function (data) {
                    $('body').addClass('hidelotnumber');
                    $('#nichtgewonnen_modaldialog').modal({backdrop: 'static', keyboard: false})
                    $('#nichtgewonnen_modaldialog').modal('show');
                },
                complete: function () {
                }
            });    
        } else {
            formdata.append('status', indicatedSegment.text);
            $.ajax({
                url: '/ajax_spieler_save',
                type: 'POST',
                data: formdata,
                processData: false,
                contentType: false,
                beforeSend: function () {
                },
                success: function (data) {
                    $('#losnummer').html(data.losnummer);
                    $('#gewonnen_modaldialog').modal({backdrop: 'static', keyboard: false})
                    $('#gewonnen_modaldialog').modal('show');
                },
                complete: function () {
                }
            });
        }
    }


    $(document).ready(function () {
        resize();
        $(window).on("resize", function () {
            resize();
        });
    });

    function resize() {
        let size = $(window).height() - $("#canvas").offset().top - Math.abs($("#canvas").outerHeight(true) - $("#canvas").outerHeight());
        console.log("CANVAS----" + size);
        $("#canvas").outerHeight();
    }


    $('.sidebarBtn,.spielen').click(function () {
        $('.sidebar').toggleClass('sidebar-active');
        $('.sidebarBtn').toggleClass('toggle');
        if ($("#sidebar-backdrop").hasClass("active")) {
            $("#sidebar-backdrop").removeClass("active");
        } else {
            $("#sidebar-backdrop").addClass("active");
        }
    });

    $('.losnummerzeigen').click(function () {
        location.reload();
    });
    @if(session('losnummer') =='')
    $('form#ajax_spieler_save')
        .formValidation({
            message: '<div class="text-danger">Kann nicht leer sein</div>',
            feedbackIcons: {
                valid: 'icon-checkmark text-success error-icon-right-1 ',
                invalid: 'icon-cancel-circle2 text-danger error-icon-right-1 ',
                validating: 'icon-checkmark text-success error-icon-right-1 '
            },
            fields: {
                anrede: {
                    validators: {
                        notEmpty: {}
                    }
                },
                vorname: {
                    validators: {
                        notEmpty: {}
                    }
                },
                nachname: {
                    validators: {
                        notEmpty: {}
                    }
                },
                handy: {
                    validators: {
                        notEmpty: {},
                        phone: {
                            country: 'DE',
                            message: 'Bitte geben Sie eine gültige Handynummer an'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {},
                        emailAddress: {
                            message: 'Bitte geben Sie eine gültige E-Mail-Adresse an'
                        }
                    }
                },
                impressumform: {
                    validators: {
                        notEmpty: {
                            message: ''
                        }
                    }
                },
                datenschutzform: {
                    validators: {
                        notEmpty: {
                            message: ''
                        }
                    }
                },

            }
        })
        .on('success.form.fv', function (e) {
            $('.sidebarBtn').toggleClass('toggle');
            $('.sidebar').toggleClass('sidebar-active');
            if ($("#sidebar-backdrop").hasClass("active")) {
                $("#sidebar-backdrop").removeClass("active");
            } else {
                $("#sidebar-backdrop").addClass("active");
            }
            startSpin();
            e.preventDefault();
            formdata = new FormData(this);
            console.log(formdata);

        });
    @endif

</script>