<footer class="text-center container-fluid mb-2">
    <div class="row">
        <div class="col-lg-12">
            <div class="center-block">
                <span class="font-weight-light footerlink" id="teilnahme">
                    Teilnahmebedingungen
                </span>
                <span>|</span>
                <span class="font-weight-light footerlink" id="impressum">
                    Impressum
                </span>
                <span>|</span>
                <span class="font-weight-light footerlink" id="datenschutz">
                    Datenschutz
                </span>
                <span>|</span>
                <span class="font-weight-light footerlink" id="kontakt">
                    Kontakt
                </span>
            </div>
        </div>
    </div>


</footer>
<div class="container-fluid footcontain">
    <div class="row text-left">
        <div class="col-lg-12 impressum d-none footercontent">
            <h6 class="text-uppercase pb-4">IMPRESSUM</h6>
            <p>SP-Service Plattform GmbH<br>
                Kornwestheimerstr. 179<br>
                70825 Münchingen</p>
            <p>
                Telefon +49 5241 23318311<br>
                Fax +49 711 22086780<br>
                Email info@sp-plattform.de
            </p>
            <h6 class="pt-5 mt-5">Haftung für Inhalte</h6>
            <p>
                Als Diensteanbieter sind wir gemäß § 7 Abs.1
                TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10
                TMG
                sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde
                Informationen
                zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.
                Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen
                bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis
                einer
                konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir
                diese
                Inhalte umgehend entfernen.

            <h6>Haftung für Links</h6>
            <p>
                Unser Angebot enthält Links zu externen Websites Dritter, auf
                deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr
                übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der
                Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche
                Rechtsverstöße
                überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente
                inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer
                Rechts- verletzung
                nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.
            </p>

            <h6>Urheberrecht</h6>
            <p>
                Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem
                Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der
                Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers.
                Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet.
            </p>
            <p>Soweit
                die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter
                beachtet.
                Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine
                Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden
                von
                Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen. Soweit die Inhalte auf dieser Seite
                nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden
                Inhalte
                Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam
                werden,
                bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir
                derartige
                Inhalte umgehend entfernen.
            </p>
            <h6>Sponsoren</h6>
            <p>
                BCV PLUS Versicherungs-Generalagentur GmbH & Co. KG<br>
                Bosfelder Weg 7<br>
                33378 Rheda-Wiedenbrück
            </p>
            <h6>Konzeption, Gestaltung und Umsetzung</h6>
            <p>
                BERNET COMMUNICATON GmbH <br>
                Heilbronner Straße 326<br>
                70469 Stuttgart<br>
                T +49 711 23960-0 <br>
                M <a href="mailto:contact@bernetbrands.com"><u>contact@bernetbrands.com</u></a>
            </p>
        </div>
        <div class="col-lg-12 teilnahme d-none footercontent">
            <h6 class="text-uppercase pb-4">Teilnahmebedingungen</h6>
            Für die Teilnahme ist ein Mindestalter von 16 Jahren erforderlich. Teilnahmeberechtigt ist jeder, mit Ausnahme der Mitarbeiter von SP-Service Plattform GmbH, Vodafone GmbH und Vodafone Kabel Deutschland GmbH und deren Angehörige. Um zu erfahren, ob das Los gewonnen hat, ruft der Teilnehmer die o. g. Nummer an oder besucht einen der fünf teilnehmenden Vodafone Shops: Kiel, Speyer, Wolfsburg, Berlin Hermannsplatz und Weiden
           <!-- Für die Teilnahme ist ein Mindestalter von 16 Jahren erforderlich. Teilnahmeberechtigt
            ist jeder, mit Ausnahme der Mitarbeiter von SP-Service Plattform GmbH, Vodafone GmbH und Vodafone Kabel
            Deutschland GmbH und deren Angehörige. Um zu erfahren, ob das Los gewonnen hat, ruft der Teilnehmer die
            kostenpflichtige, o. g. Nummer an oder besucht einen der fünf teilnehmenden Vodafone Shops: Kiel, Speyer,
            Wolfsburg, Berlin Hermannsplatz und Weiden.-->

            <h6 class="pt-3 mb-0">Filiale Speyer</h6>
            <p>
                Postplatz 5<br>
                67346 Speyer
            </p>
            <h6 class="pt-3 mb-0">Du erreichst uns werktags von 10:00 bis 18:00 Uhr</h6>
            
            <p class="pt-3 mb-0">In diesen Shops
                werden auch die Gewinne ausgegeben. Sollte ein Gewinner nicht die Möglichkeit haben, den Gewinn
                abzuholen,
                wird er ihm zugeschickt. Die Umwandlung der Sachpreise in Geld sowie der Rechtsweg sind
                ausgeschlossen.</p>

        </div>
        <div class="col-lg-12 datenschutz d-none footercontent">
            <h6 class="text-uppercase pb-4">Datenschutz</h6>

Informationspflichten nach Art. 12 ff. EU-DSGVO<br><br>

I.  Name und Kontaktdaten des Verantwortlichen<br>
II. Kontaktdaten des Datenschutzbeauftragten<br>
III.    Datenverarbeitungen auf unserer Webseite<br>
1.  Webseitenfunktionen<br>
i.  Bereitstellung der Webseite und Erstellung von Logfiles<br>
ii. Technisch-notwendige Cookies<br>
iii.    E-Mail-Kontakt<br>
2.  Bewerbungsverfahren<br>
3.  Datenschutz und Recht<br>
i.  Betroffeneneingaben nach Art. 12 ff. EU-DSGVO<br>
ii. Rechtsverteidigung und -durchsetzung<br>
IV. Empfängerkategorien<br>
V.  Drittlandübermittlung<br>
VI. Ihre Rechte<br><br><br>

<div class="sub_hl">I.  Name und Kontaktdaten des Verantwortlichen</div>

Ihr Ansprechpartner als Verantwortlicher im Sinne der Europäischen Datenschutz-Grundverordnung („EU-DSGVO“) und anderer nationaler Datenschutzgesetze der Mitgliedsstaaten sowie sonstiger datenschutzrechtlicher Bestimmungen ist:<br><br>

SP Service-Plattform GmbH<br>
Hohenzollernstraße 76<br>
80801 München
Telefon: +49 7150 / 35 14 72 10<br>
Fax: +49 7150 / 35 14 72 19<br>
E-Mail: info@sp-plattform.de<br><br>

(im Folgenden als „wir“, „uns“ oder „unser“ bezeichnet)<br><br>


<div class="sub_hl">II. Kontaktdaten des Datenschutzbeauftragten</div>

Der Schutz Ihrer persönlichen Daten hat für uns einen hohen Stellenwert. Um dieser Bedeutung Ausdruck zu verleihen, haben wir ein auf Datenschutz und Datensicherheit spezialisiertes Beratungsunternehmen mit der Übernahme dieser zentralen Themen beauftragt. Auch unser Datenschutzbeauftragter kommt aus diesem sehr erfahrenen Expertenkreis.<br><br> 

Wir werden beraten durch:<br>

MAGELLAN Rechtsanwälte, Brienner Straße 11, 80333 München / www.magellan-datenschutz.de<br><br>

Bitte wenden Sie sich in allen Fragen rund um das Thema Datenschutz und Datensicherheit bei uns direkt an unseren Datenschutzbeauftragten von MAGELLAN Rechtsanwälte:<br><br>

E-Mail: datenschutz_sp@magellan-rechtsanwaelte.de / Tel.: +49 5241 / 233 183 11<br><br>


<div class="sub_hl">III.    Datenverarbeitungen auf unserer Webseite</div><br>
<strong>1.  Webseitenfunktionen</strong><br><br>

<strong>i.  Bereitstellung der Webseite und Erstellung von Logfiles</strong><br><br>

<strong>a.  Rechtsgrundlage</strong><br><br>

Rechtsgrundlage für die Verarbeitung Ihrer personenbezogenen Daten im Rahmen der Bereitstellung der Webseite und der Erstellung von Logfiles ist Art. 6 Abs. 1 lit. f EU-DSGVO.<br><br>

<strong>b.  Zweck</strong><br><br>

Die vorübergehende Speicherung Ihrer personenbezogenen Daten durch uns ist notwendig, um eine Auslieferung der Webseite an Ihren Rechner zu ermöglichen. Hierfür müssen Ihre personenbezogenen Daten für die Dauer der Sitzung gespeichert werden.<br><br>

Die Speicherung Ihrer personenbezogenen Daten in Logfiles erfolgt, um die Funktionsfähigkeit der Webseite sicherzustellen. Zudem dienen uns Ihre personenbezogenen Daten zur Sicherstellung der Sicherheit unserer informationstechnischen Systeme. Eine anderweitige Verarbeitung Ihrer personenbezogenen Daten findet nicht statt.<br><br>

<strong>c.  Speicherdauer</strong><br><br>

Ihre personenbezogenen Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Verarbeitung nicht mehr erforderlich sind. Im Falle der Erfassung Ihrer personenbezogenen Daten zur Bereitstellung der Webseite ist dies gegeben, sobald Sie die Webseite verlassen haben.<br><br>

Im Falle der Speicherung Ihrer personenbezogenen Daten in Logfiles werden diese nach spätestens 30 Tagen gelöscht. Erfolgt eine darüberhinausgehende Speicherung, so werden Ihre personenbezogenen Daten anonymisiert, sodass eine Zuordnung nicht mehr möglich ist.<br><br>

<strong>d.  Widerspruchs- und Beseitigungsmöglichkeit</strong><br><br>

Die Verarbeitung Ihrer personenbezogenen Daten zur Bereitstellung der Webseite und die Speicherung Ihrer personenbezogenen Daten in Logfiles ist für den Betrieb der Webseite zwingend erforderlich. Es besteht für Sie folglich keine Widerspruchsmöglichkeit.<br><br>

<strong>ii. Technisch-notwendige Cookies</strong><br><br>

<strong>a.  Rechtsgrundlage</strong><br><br>

Rechtsgrundlage für die Verarbeitung Ihrer personenbezogenen Daten im Rahmen der Verwendung von technisch-notwendigen Cookies ist Art. 6 Abs. 1 lit. f EU-DSGVO.<br><br>

<strong>b.  Zweck</strong><br><br>

Die Verwendung von technisch-notwendigen Cookies dient dazu, Ihnen die Nutzung unserer Webseite zu vereinfachen. Einige Funktionen unserer Webseite können ohne den Einsatz von Cookies nicht angeboten werden. Für diese ist es erforderlich, dass Ihr Browser auch nach einem Seitenwechsel wiedererkannt wird. Eine anderweitige Verarbeitung Ihrer personenbezogenen Daten findet nicht statt.<br><br>

<strong>c.  Speicherdauer</strong><br><br>

Ihre personenbezogenen Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Verarbeitung nicht mehr erforderlich sind; dies ist insbesondere im Fall des Verlassens der Webseite gegeben.<br><br>

<strong>d.  Widerspruchs- und Beseitigungsmöglichkeit</strong><br><br>

Cookies werden im Erlaubnisfalle auf Ihrem Rechner gespeichert und von diesem an unsere Webseite übermittelt. Daher haben Sie die volle Kontrolle über die Verwendung von Cookies.<br><br>

Durch eine Änderung der Einstellungen in Ihrem Browser können Sie die Übertragung von Cookies deaktivieren oder einschränken. Bereits gespeicherte Cookies können Sie jederzeit löschen. Dies kann auch automatisiert erfolgen. Werden Cookies für unsere Webseite deaktiviert, können möglicherweise nicht mehr alle Funktionen der Webseite vollumfänglich genutzt werden.<br><br>

Die Übermittlung von Flash-Cookies lässt sich nicht über die Einstellungen Ihres Browsers unterbinden. Hierzu sind entsprechende Änderungen der Einstellung des Adobe Flash Players erforderlich.<br><br>

<strong>iii.    E-Mail-Kontakt</strong><br><br>

<strong>a.  Rechtsgrundlage</strong> <br><br>

Rechtsgrundlage für die Verarbeitung Ihrer personenbezogenen Daten, die im Rahmen der Kontaktaufnahme übermittelt werden, ist Art. 6 Abs. 1 lit. f EU-DSGVO. Zielt die Kontaktaufnahme auf den Abschluss eines Vertrages ab, so ist Art. 6 Abs. 1 lit. b EU-DSGVO eine zusätzliche Rechtsgrundlage für die Verarbeitung Ihrer personenbezogenen Daten.<br><br>

<strong>b.  Zweck</strong> <br><br>

Die Verarbeitung Ihrer personenbezogenen Daten im Falle einer Kontaktaufnahme dient uns allein zur Bearbeitung Ihres Anliegens.<br><br>

<strong>c.  Speicherdauer</strong><br><br>

Ihre personenbezogenen Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Verarbeitung nicht mehr erforderlich sind. Für die personenbezogenen Daten, die im Rahmen der Kontaktaufnahme übersendet wurden, ist dies der Fall, wenn Ihr Anliegen bearbeitet wurde und gesetzlichen Aufbewahrungsfristen einer Löschung nicht entgegenstehen.<br><br>

<strong>d.  Widerspruchs- und Beseitigungsmöglichkeit</strong><br><br>

Sie haben jederzeit die Möglichkeit, der Verarbeitung Ihrer personenbezogenen Daten im Rahmen der Kontaktaufnahme für die Zukunft zu widersprechen. In diesem Fall können wir Ihr Anliegen jedoch nicht weiterbearbeiten. Alle personenbezogenen Daten, die im Zuge der Kontaktaufnahme gespeichert wurden, werden in diesem Fall gelöscht, es sei denn, dass gesetzliche Aufbewahrungsfristen einer Löschung entgegenstehen.<br><br>

<strong>2.  Bewerbungsverfahren</strong><br><br>

<strong>i.  Personenbezogene Daten</strong><br><br>

Soweit wir für Verarbeitungsvorgänge personenbezogener Daten eine Einwilligung von Ihnen einholen, dient uns Art. 6 Abs. 1 lit. a EU-DSGVO, Art. 88 Abs. 1 EU-DSGVO i.V.m. § 26 Abs. 2 BDSG als Rechtsgrundlage.<br><br>

Bei der Verarbeitung von personenbezogenen Daten, die zur Begründung des Arbeitsvertrages erforderlich ist, dient uns Art. 6 Abs. 1 lit. b EU-DSGVO, Art. 88 Abs. 1 EU-DSGVO i.V.m. § 26 Abs. 1 BDSG, § 611a BGB als Rechtsgrundlage.<br><br>

Soweit eine Verarbeitung personenbezogener Daten zur Erfüllung einer rechtlichen Verpflichtung erforderlich ist, welcher wir unterliegen, dient uns Art. 6 Abs. 1 lit. c EU-DSGVO als Rechtsgrundlage.<br><br>

Für den Fall, dass lebenswichtige Interessen von Ihnen oder einer anderen natürlichen Person eine Verarbeitung personenbezogener Daten erforderlich machen, dient uns Art. 6 Abs. 1 lit. d EU-DSGVO als Rechtsgrundlage.<br><br>

Ist die Verarbeitung zur Wahrung eines berechtigten Interesses von uns oder eines Dritten erforderlich und überwiegen Ihre Interessen, Grundrechte und Grundfreiheiten das erstgenannte Interesse nicht, so dient uns Art. 6 Abs. 1 lit. f EU-DSGVO als Rechtsgrundlage für die Verarbeitung.<br><br>

<strong>ii. Besondere Kategorien personenbezogener Daten</strong><br><br>

Soweit wir für die Verarbeitung besonderer Kategorien personenbezogener Daten (Art. 9 Abs. 1 EU-DSGVO) wie unter anderem der Religionszugehörigkeit, Nationalität sowie der Gesundheitsdaten eine Einwilligung von Ihnen einholen, dient Art. 9 Abs. 2 lit. a EU-DSGVO als Rechtsgrundlage.<br><br>

Wenn die Verarbeitung besonderer Kategorien personenbezogener Daten erforderlich ist, damit wir die uns aus dem Arbeitsrecht und dem Recht der sozialen Sicherheit und des Sozialschutzes erwachsenden Rechte ausüben können und unseren diesbezüglichen Pflichten nachkommen, folgt die Rechtsgrundlage für die Verarbeitung aus Art. 9 Abs. 2 lit. b EU- DSGVO, Art. 88 Abs. 1 EU-DSGVO i.V.m. § 26 Abs. 3 BDSG.<br><br>

Sofern die Verarbeitung besonderer Kategorien personenbezogener Daten zum Schutz lebenswichtiger Interessen erforderlich ist, folgt die Rechtsgrundlage für die Verarbeitung aus Art. 9 Abs. 2 lit. c EU-DSGVO.<br><br>

Bezieht sich die Verarbeitung auf besondere Kategorien personenbezogener Daten, die von Ihnen offensichtlich öffentlich gemacht wurden, ergibt sich die Rechtsgrundlage aus Art. 9 Abs. 2 lit. e EU-DSGVO.<br><br>

Falls die Verarbeitung besonderer Kategorien personenbezogener Daten für Zwecke der Gesundheitsvorsorge, der Arbeitsmedizin oder für die Beurteilung der Arbeitsfähigkeit erforderlich ist, folgt die Rechtsgrundlage aus Art. 9 Abs. 2 lit. h EU-DSGVO.<br><br>

<strong>iii.    Zwecke</strong><br><br>

Die Verarbeitung Ihrer personenbezogenen Daten erfolgt zum Zwecke der Begründung des Beschäftigungsverhältnisses, insbesondere zur Erfüllung arbeitsvertraglicher, gesetzlicher, sofern vorliegend kollektivvertraglicher, sowie sozialversicherungsrechtlicher Verpflichtungen.<br><br>

<strong>iv. Speicherdauer</strong><br><br>

Ihre personenbezogenen Daten werden gelöscht oder gesperrt, sobald der Zweck der Speicherung entfällt. Eine Speicherung kann darüber hinaus erfolgen, wenn dies durch den europäischen oder nationalen Gesetzgeber in unionsrechtlichen Verordnungen, Gesetzen oder sonstigen Vorschriften, denen wir unterliegen, vorgesehen wurde. Eine Sperrung oder Löschung der Daten erfolgt auch dann, wenn eine durch die genannten Normen vorgeschriebene Speicherfrist abläuft, es sei denn, dass eine Erforderlichkeit zur weiteren Speicherung der Daten für einen Vertragsabschluss oder eine Vertragserfüllung besteht.<br><br>

Danach speichern wir Ihre Daten unter anderem für folgende Zeiträume:<br>

<ul>
<li>Bewerbungsunterlagen,-daten, nach Entscheidung über Nichtbesetzung, bis zu 6 Monate, Diskriminierungsbeweislast, Frist §§ 21 Abs. 5, 22 AGG (Allgemeines Gleichbehandlungsgesetz)</li>
<li>Bewerbungsunterlagen ansonsten: Bei Auflösung, Beendigung des Arbeitsverhältnisses</li>
</ul>

<strong>v.  Widerspruchs- und Beseitigungsmöglichkeit</strong><br><br>

Die Verarbeitung Ihrer personenbezogenen Daten im Rahmen des Bewerbungsverfahrens ist für die Begründung des Beschäftigungsverhältnisses zwingend erforderlich. Es besteht folglich für Sie keine Widerspruchsmöglichkeit.<br><br>

Sofern die Verarbeitung Ihrer personenbezogenen Daten auf Grundlage einer Einwilligung erfolgt, haben Sie jederzeit die Möglichkeit, Ihre Einwilligung zu widerrufen.<br><br>

<strong>3.  Datenschutz und Recht</strong><br><br>

<strong>i.  Betroffeneneingaben nach Art. 12 ff. EU-DSGVO</strong><br><br>

<strong>a.  Rechtsgrundlage</strong><br><br>

Die Rechtsgrundlage für die Verarbeitung Ihrer personenbezogenen Daten im Rahmen der Bearbeitung Ihrer datenschutzrechtlichen Anfrage („Betroffeneneingabe“) ist Art. 6 Abs. 1 lit. c i.V.m. Art. 12 ff. EU-DSGVO. Die Rechtsgrundlage für die anschließende Dokumentation der gesetzeskonformen Bearbeitung von Betroffeneneingabe ist Art. 6 Abs. 1 lit. f EU-DSGVO.<br><br>

<strong>b.  Zweck</strong><br><br>

Der Zweck der Verarbeitung Ihrer personenbezogenen Daten im Rahmen der Bearbeitung von Betroffeneneingaben ist die Beantwortung Ihrer datenschutzrechtlichen Anfrage. Die anschließende Dokumentation der gesetzeskonformen Bearbeitung der jeweiligen Betroffeneneingabe dient zur Erfüllung der gesetzlich geforderten Rechenschaftspflicht, Art. 5 Abs. 2 EU-DSGVO. <br><br>

<strong>c.  Speicherdauer</strong><br><br>

Ihre personenbezogenen Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Verarbeitung nicht mehr erforderlich sind. Im Falle der Bearbeitung von Betroffeneneingaben ist dies gemäß § 41 BDSG i.V.m. § 31 Abs. 2 Nr. 1 OWiG drei Jahren nach Ende des jeweiligen Vorgangs gegeben.<br><br>

<strong>d.  Widerspruchs- und Beseitigungsmöglichkeit</strong><br><br>

Sie haben jederzeit die Möglichkeit, der Verarbeitung Ihrer personenbezogenen Daten im Rahmen der Bearbeitung von Betroffeneneingaben für die Zukunft zu widersprechen. In diesem Fall können wir Ihre datenschutzrechtliche Anfrage jedoch nicht weiterbearbeiten. <br><br>

Die Dokumentation der gesetzeskonformen Bearbeitung der jeweiligen Betroffeneneingabe ist zwingend erforderlich. Es besteht folglich für Sie keine Widerspruchsmöglichkeit. <br><br>

<strong>ii. Rechtsverteidigung und -durchsetzung</strong><br><br>

<strong>a.  Rechtsgrundlage</strong><br><br>

Die Rechtsgrundlage für die Verarbeitung Ihrer personenbezogenen Daten im Rahmen der Rechtsverteidigung und -durchsetzung ist Art. 6 Abs. 1 lit. f EU-DSGVO.<br><br>

<strong>b.  Zweck</strong><br><br>

Der Zweck der Verarbeitung Ihrer personenbezogenen Daten im Rahmen der Rechtsverteidigung und -durchsetzung ist die Abwehr von unberechtigter Inanspruchnahme sowie die rechtliche Durchsetzung und Geltendmachung von Ansprüchen und Rechten.<br><br>

<strong>c.  Speicherdauer</strong><br><br>

Ihre personenbezogenen Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Verarbeitung nicht mehr erforderlich sind.<br><br>

<strong>d.  Widerspruchs- und Beseitigungsmöglichkeit</strong><br><br>

Die Verarbeitung Ihrer personenbezogenen Daten im Rahmen der Rechtsverteidigung und -durchsetzung ist für die Rechtsverteidigung und -durchsetzung zwingend erforderlich. Es besteht folglich für Sie keine Widerspruchsmöglichkeit.<br><br>


<div class="sub_hl">IV. Empfängerkategorien</div>

Innerhalb unseres Unternehmens erhalten diejenigen Stellen und Abteilungen personenbezogene Daten, die diese zur Erfüllung der zuvor genannten Zwecke brauchen. Daneben bedienen wir uns zum Teil unterschiedlicher Dienstleister und übermitteln Ihre personenbezogenen Daten an weitere vertrauenswürdige Empfänger. Diese können z.B. sein:<br>

<ul>
<li>Banken</li>
<li>Scan-Service</li>
<li>IT-Dienstleister</li>
<li>Rechtsanwälte und Gerichte</li>
</ul>

<div class="sub_hl">V.  Drittlandübermittlung</div>

Im Rahmen der Verarbeitung Ihrer personenbezogenen Daten kann es vorkommen, dass wir Ihre personenbezogenen Daten an vertrauenswürdige Dienstleister in Drittländern übermitteln. Drittländer sind Länder, die außerhalb der Europäischen Union (EU) oder des Europäischen Wirtschaftsraums (EWR) liegen.<br><br>

Dabei arbeiten wir nur mit solchen Dienstleistern zusammen, die uns geeignete Garantien für die Sicherheit Ihrer personenbezogenen Daten geben und garantieren können, dass Ihre personenbezogenen Daten nach den strengen europäischen Datenschutzstandards verarbeitet werden. Eine Kopie dieser geeigneten Garantien kann bei uns vor Ort eingesehen werden.<br><br>

Wenn wir personenbezogene Daten in Drittländern übermitteln, erfolgt dies auf der Grundlage eines sogenannten Angemessenheitsbeschlusses der Europäischen Kommission, oder, wenn ein solcher Beschluss nicht vorliegt, auf der Grundlage von sogenannten Standarddatenschutzklauseln, die ebenfalls von der Europäischen Kommission erlassen wurden. <br><br>

Vorliegend kann nicht ausgeschlossen werden, dass wir personenbezogene Daten an Dienstleister in den USA übermitteln. Diese Dienstleister sind nach dem „EU-U.S. and Swiss-U.S. Privacy Shield Framework“ zertifiziert. Nähere Informationen zum „EU-U.S. and Swiss-U.S. Privacy Framework“ finden Sie unter: www.privacyshield.gov<br><br>


<div class="sub_hl">VI. Ihre Rechte</div>

Folgende Rechte stehen Ihnen uns gegenüber zu:<br><br>

<strong>1.  Recht auf Auskunft</strong><br><br>

Sie haben ein Recht auf Auskunft darüber, ob und welche personenbezogenen Daten von Ihnen von uns verarbeitet werden. In diesem Fall informieren wir Sie zusätzlich über<br><br>

(1) den Verarbeitungszweck;<br>
(2) die Datenkategorien;<br>
(3) die Empfänger Ihrer personenbezogenen Daten;<br>
(4) die geplante Speicherdauer bzw. die Kriterien für die geplante Speicherdauer;<br>
(5) Ihre weiteren Rechte; <br>
(6) sofern wir Ihre personenbezogenen Daten nicht von Ihnen mitgeteilt wurden: Alle verfügbaren Informationen über deren Herkunft;<br>
(7) sofern vorhanden: das Bestehen einer automatisierten Entscheidungsfindung sowie Informationen über die involvierte Logik, die Tragweite und die angestrebten Auswirkungen der Verarbeitung.<br><br>

<strong>2.  Recht auf Berichtigung</strong><br><br>

Sie haben ein Recht auf Berichtigung und/oder Vervollständigung, sofern Ihre von uns verarbeiteten personenbezogenen Daten unrichtig oder unvollständig sind.<br><br>

<strong>3.  Recht auf Einschränkung der Verarbeitung</strong><br><br>

Sie haben ein Recht auf Einschränkung der Verarbeitung, sofern<br><br>

(1) wir die Richtigkeit Ihrer von uns verarbeiteten personenbezogenen Daten überprüfen;<br>
(2) die Verarbeitung Ihrer personenbezogenen Daten unrechtmäßig ist;<br>
(3) Sie Ihre von uns verarbeiteten personenbezogenen Daten nach Zweckwegfall zur Rechtsverfolgung benötigen;<br>
(4) Sie Widerspruch gegen die Verarbeitung Ihrer personenbezogenen Daten eingelegt haben und wir diesen Widerspruch prüfen.<br><br>

<strong>4.  Recht auf Löschung</strong><br><br>

Sie haben ein Recht auf Löschung, sofern<br><br>

(1) wir Ihre personenbezogenen Daten für ihren ursprünglichen Verwendungszweck nicht mehr benötigen;<br>
(2) Sie Ihre Einwilligung widerrufen und es keine weitere Rechtsgrundlage für die Verarbeitung ihrer personenbezogenen Daten gibt;<br>
(3) Sie Widerspruch gegen die Verarbeitung Ihrer personenbezogenen Daten einlegen und - sofern es sich nicht um Direktmarketing handelt - keine vorrangigen Gründe für die Weiterverarbeitung vorliegen;<br>
(4) die Verarbeitung Ihrer personenbezogenen Daten unrechtmäßig ist;<br>
(5) die Löschung Ihrer personenbezogenen Daten gesetzlich gefordert ist;<br>
(6) Ihre personenbezogenen Daten als Minderjähriger für Dienste der Informationsgesellschaft erhoben wurden.<br><br>

<strong>5.  Recht auf Unterrichtung</strong><br><br>

Sofern Sie Ihr Recht auf Berichtigung, Löschung oder Einschränkung der Verarbeitung geltend gemacht haben, werden wir allen Empfängern Ihrer personenbezogenen Daten, diese Berichtigung, Löschung der Daten oder Einschränkung der Verarbeitung mitteilen.<br><br>

<strong>6.  Recht auf Datenübertragbarkeit</strong><br><br>

Sie haben ein Recht Ihre von uns auf Grundlage einer Einwilligung oder zur Vertragsdurchführung verarbeiteten personenbezogenen Daten in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten und an einen anderen Verantwortlichen zu übermitteln. Sofern dies technisch machbar ist, haben Sie das Recht, dass wir diese Daten direkt an einen anderen Verantwortlichen übermitteln.<br><br>

<strong>7.  Widerspruchsrecht</strong><br><br>

Sie haben im Fall von besonderen Gründen ein Recht auf Widerspruch gegen die Verarbeitung Ihrer personenbezogenen Daten. In diesem Fall verarbeiten wir Ihre personenbezogenen Daten nicht mehr, es sei denn, wir können zwingende schutzwürdige Gründe für die Verarbeitung nachweisen.<br><br>

Im Fall einer Verarbeitung Ihrer personenbezogenen Daten zum Zwecke des Direktmarketings haben Sie jederzeit ein Recht auf Widerspruch.<br><br>

<strong>8.  Recht auf Widerruf</strong><br><br>

Sie haben das Recht, eine uns abgegebene Einwilligung jederzeit zu widerrufen. Durch den Widerruf der Einwilligung wird die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung nicht berührt.<br><br>

9<strong>.  Recht auf Beschwerde bei einer Aufsichtsbehörde</strong><br><br>

Unbeschadet eines anderweitigen verwaltungsrechtlichen oder gerichtlichen Rechtsbehelfs steht Ihnen das Recht auf Beschwerde bei der zuständigen Aufsichtsbehörde zu, wenn Sie der Ansicht sind, dass die Verarbeitung Ihrer personenbezogenen Daten durch uns gegen die EU-DSGVO verstößt.<br><br>

Zuständige Aufsichtsbehörde für uns ist:<br>

Bayerisches Landesamt für Datenschutzaufsicht (BayLDA)<br>
Promenade 18<br>
91522 Ansbach<br>
Tel.: +49 981 180093-0<br>
Fax: +49 981 180093-800<br>
E-Mail: poststelle@lda.bayern.de<br>
<br><br>




Für Rückfragen steht Ihnen unser Datenschutzbeauftragter jederzeit gerne zur Verfügung.
        </div>
        <div class="col-lg-12 kontakt d-none footercontent">
            <div class="row">
                <div class="col-lg-12">
                    <h6 class="text-uppercase pb-3">KONTAKT</h6>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-2">
                    <h6 class="mb-0">Unternehmenssitz:</h6>
                    <p>SP-Service Plattform GmbH <br>
                        Kornwestheimerstr. 179<br>
                        70825 Münchingen<br>
                        Fon +49 7150 35147210
                    </p>
                </div>
                <div class="col-lg-2">
                    <h6 class="mb-0">Personalwesen:</h6>
                    <p>SP-Service Plattform GmbH <br>
                        Winkelstrasse 2<br>
                        33332 Gütersloh<br>
                        Fon +49 5241 23318311 <br>
                        Fax +49 711 22086780 <br>
                        Email info@sp-plattform.de
                    </p>
                </div>
                <div class="col-lg-2">
                    <h6 class="mb-0">Operatives Geschäft:</h6>
                    <p>SP-Service Plattform GmbH <br>
                        Kornwestheimer Str. 179<br>
                        70825 Korntal-Münchingen
                    </p>
                </div>
                <div class="col-lg-3">
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $("#impressum").click(function () {
        $('.footerlink').removeClass('active');
        $(this).addClass('active');
        $('.footerlink').removeClass('font-weight-bold');
        $('.footerlink').addClass('font-weight-light');
        $(this).removeClass('font-weight-light');
        $(this).addClass('font-weight-bold');
        $('.impressum').delay(500).slideUp(500, function () {
            $(this).show();
            $('.impressum').removeClass('d-none');
            $('.teilnahme').addClass('d-none');
            $('.datenschutz').addClass('d-none');
            $('.kontakt').addClass('d-none');
            if ($('.impressum'))
                $('html, body').animate({
                    scrollTop: $("div.impressum").offset().top
                }, 1000)
        });
    });
    $("#teilnahme").click(function () {
        $('.footerlink').removeClass('active');
        $(this).addClass('active');
        $('.footerlink').removeClass('font-weight-bold');
        $('.footerlink').addClass('font-weight-light');
        $(this).removeClass('font-weight-light');
        $(this).addClass('font-weight-bold');
        $('.teilnahme').delay(500).slideUp(500, function () {
            $(this).show();
            $('.teilnahme').removeClass('d-none');
            $('.impressum').addClass('d-none');
            $('.datenschutz').addClass('d-none');
            $('.kontakt').addClass('d-none');
            if ($('.teilnahme'))
                $('html, body').animate({
                    scrollTop: $("div.teilnahme").offset().top
                }, 1000)
        });
    });
    $("#datenschutz").click(function () {
        $('.footerlink').removeClass('active');
        $(this).addClass('active');
        $('.footerlink').removeClass('font-weight-bold');
        $('.footerlink').addClass('font-weight-light');
        $(this).removeClass('font-weight-light');
        $(this).addClass('font-weight-bold');
        $('.datenschutz').delay(500).slideUp(500, function () {
            $(this).show();
            $('.datenschutz').removeClass('d-none');
            $('.impressum').addClass('d-none');
            $('.teilnahme').addClass('d-none');
            $('.kontakt').addClass('d-none');
            if ($('.datenschutz'))
                $('html, body').animate({
                    scrollTop: $("div.datenschutz").offset().top
                }, 1000)
        });
    });
    $("#kontakt").click(function () {
        $('.footerlink').removeClass('active');
        $(this).addClass('active');
        $('.footerlink').removeClass('font-weight-bold');
        $('.footerlink').addClass('font-weight-light');
        $(this).removeClass('font-weight-light');
        $(this).addClass('font-weight-bold');
        $('.kontakt').delay(500).slideUp(500, function () {
            $(this).show();
            $('.kontakt').removeClass('d-none');
            $('.datenschutz').addClass('d-none');
            $('.teilnahme').addClass('d-none');
            $('.impressum').addClass('d-none');
            if ($('.kontakt'))
                $('html, body').animate({
                    scrollTop: $("div.kontakt").offset().top
                }, 1000)
        });
    });
</script>