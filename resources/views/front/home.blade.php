<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SPS Gewinnspiel</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="{{ asset('js/spielcircle.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
    <script src="{{ asset("js/validation/formValidation.min.js")}}"></script>
    <script src="{{ asset("js/validation/formValidation-bootstrap.min.js")}}"></script>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/de_DE/sdk.js"></script>
    <?php

    $useragent = $_SERVER['HTTP_USER_AGENT'];
    if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|android|ipad|playbook|silk|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
        $ismobile = true;
    } else {
        $ismobile = false;
    }

    ?>
    @if(session('losnummer') =='')
        <script>

            function statusChangeCallback(response) {
                console.log(response);
                if (response.status === 'connected') {
                    testAPI();
                }
            }

            function checkLoginState() {
                FB.getLoginStatus(function (response) {
                    statusChangeCallback(response);
                });
            }

            window.fbAsyncInit = function () {
                FB.init({
                    appId: '778818632905479',
                    cookie: true,
                    xfbml: true,
                    version: 'v8.0'
                });

                FB.getLoginStatus(function (response) {
                    statusChangeCallback(response);
                });
            };

            function testAPI() {
                FB.api('/me', function (response) {
                    $('#vorname').val(response.name);
                });
            }

            /*@if($ismobile)
            @if(session('losnummer') =='')
            setTimeout(function () {
                $('#werbung_modaldialog').modal('show');
            }, 3000);
            @endif
            @endif*/

        </script>
        <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '336175754365900');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=336175754365900&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
    @endif

</head>
<body>

<div class="container">
    <div class="content">

        <div class="rechtect-weiss"></div>
        <div class="rechtect-gelb"></div>
        <div class="rechtect-schwarz"></div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <img src="{{ asset('images/sp_logo.png') }}" class="img-fluid sp-logo"><br>
                <img src="{{ asset('images/sp_claim.png') }}" class="img-fluid sp-claim">
            </div>
            @if(session('losnummer') !='')
                <div class="mobilesep col-12"></div>
            @endif
        </div>

        <div class="row">
            <div class="col-xl-6">


                @if(session('losnummer') =='')
                    <p class="title_tablet mt-5">Gewinne jetzt dein<br>neues Smartphone</p>
                    <p class="title">Gewinne jetzt dein<br> neues Smartphone <br>im Wert von über 900 €</p>

                    <div class="unter-title">
                        Du willst ein nagelneues Smartphone? @if($ismobile) <br> @endif Einfach registrieren, mitspielen
                        und ein iPhone 11 oder ein
                        XIAOMI Mi 10 Pro gewinnen. Wir wünschen dir viel Glück!
                    </div>
                    <div class="spielen_button desktop">
                        <button type="button" class="btn spielen">
                            SPIELEN UND GEWINNEN
                        </button>
                    </div>
                @else
                    @if(session('status') !='Kein Gewinn')
                        <p></p>
                        <p class="title "> Super, herzlichen Glückwunsch!<br> Lass deine Losnummer bestätigen.
                        </p>
                        <p class="title_tablet "> Super, herzlichen Glückwunsch!<br> Lass deine Losnummer bestätigen.
                        </p>
                    @else
                        <p class="title ">Schade,<br> du hast kein Los gewonnen.</p>
                        <p class="title_tablet "> Schade,<br> du hast kein Los gewonnen.</p>
                    @endif
                @endif

                <div class="handy_image_container">
                    <img src="{{ asset('images/links_image.png') }}"
                         class="img-fluid">
                    <div class="textinimage">Du erreichst uns werktags<br>von 10:00 bis 18:00 Uhr.</div>
                </div>

                @if(session('losnummer') !='')
                    <div class="handy_image_container mobile">
                        <img src="{{ asset('images/links_image.png') }}"
                             class="img-fluid">
                        <div class="textinimage">Du erreichst uns werktags<br>von 10:00 bis 18:00 Uhr.</div>
                    </div>
                @endif

                @if(session('losnummer') !='')
                    @if(session('status') !='Kein Gewinn')
                        <span class="span-btn losnumbox desktop border-0">Deine persönliche Losnummer : <span></span>{{session('losnummer')}}</span>
                    @else
                        <span class="span-btn losnumbox desktop border-0">Sie haben schon gespielt</span>
                    @endif
                @endif
            </div>
            <div class="col-xl-6">
                <div id="canvasContainer" @if(session('losnummer') !='') class="hidemobile"
                     @endif style="position: relative; width: 300px;">
                    <canvas id="canvas" width="600" height="600"
                            data-responsiveMinWidth="180"
                            data-responsiveScaleHeight="true"
                            data-responsiveMargin="0">
                        <p style="{color: white}" align="center">Sorry, your browser doesn't support canvas. Please
                            try another.</p>
                    </canvas>
                    <img class="cursor-png" src="{{ asset('images/cursor.svg') }}"/>
                </div>
            </div>

            @if(session('losnummer') =='')
                <div class="spielen_button mobile">
                    <button type="button" class="btn spielen">
                        SPIELEN UND GEWINNEN
                    </button>
                </div>
            @endif

            @if(session('losnummer') =='')
                <div class="handy_image_container mobile bottom">
                    <img src="{{ asset('images/links_image.png') }}"
                         class="img-fluid">
                    <div class="textinimage">Sie erreichen uns Werktags<br>von 10:00 bis 18:00 Uhr</div>
                </div>
            @endif

        </div>

        @if(session('losnummer') =='')
            @include('front.sidebar')
        @endif

        <div class="row footerrow">
            <div class="col-lg-12">
                @include('front.footer')
            </div>
        </div>
    </div>
</div>

@if(session('losnummer') =='')
    <!--  Modal DIALOG -->
    <div class="modal fade" id="gewonnen_modaldialog" tabindex="-1" data-backdrop="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content bg-red">
                <button type="button" class="circle-close losnummerzeigen" data-dismiss="modal">&times;</button>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="modal_title">
                                <p>Super, herzlichen Glückwunsch!</p>
                                <p>Du hast ein Los gewonnen!</p>
                            </div>
                        </div>
                        <div class="col-lg-6 offset-lg-3">
                            <div class="modal_content">
                                <p> Und so geht’s weiter:
                                </p>
                                <p>Anrufen, Losnummer bestätigen lassen und mehr zu
                                    unseren
                                    Gewinnen
                                    erfahren.</p>
                                <p>Bis gleich!<span class="ml-2" style="font-size: 40px;top:-10px">👋</span></p>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="modal_button">
                            <span class="span-btn border-0">Deine persönliche Losnummer
                                :<span id="losnummer">{{session('losnummer')}}</span>
                            </span>
                            </div>
                        </div>
                        <div class="col-lg-12 pb-0">
                            <div class="modal_footer" style="margin-top: 20px;">
                                <p> Bitte beachte, dass die Losnummer nur 72 Stunden Gültigkeit hat.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--  Modal DIALOG -->
    <div class="modal fade" id="nichtgewonnen_modaldialog" tabindex="-1" data-backdrop="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content bg-red">
                <button type="button" class="circle-close losnummerzeigen" data-dismiss="modal">&times;</button>
                <div class="modal-body" style="padding: 14%">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="modal_title">
                                <p>Schade,<br> du hast kein Los gewonnen.</p>
                                <p style="font-size: 60px;padding-top: 21px;">😢</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="werbung_modaldialog" tabindex="-1" data-backdrop="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content bg-red">
                <button type="button" class="circle-close" data-dismiss="modal">&times;</button>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 mt-5 mb-5">
                            <img src="{{ asset('images/links_image_2.png') }}" class="img-fluid">
                        </div>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    Abbildungen Ähnlich
                </div>
            </div>
        </div>
    </div>
@endif
<!--<div class="modal-backdrop fade"></div>-->
@include('front.spinner_js')
</body>
</html>
