<div id="sidebar-backdrop"></div>
<div class="sidebar">
    {{ Form::open(array('method'=>'post', 'id' =>'ajax_spieler_save', 'enctype'=> 'multipart/form-data', 'autocomplete' => 'on' )) }}
    <input type="hidden" name="cookies" value="{{session('cookies')}}"/>
    <input type="hidden" name="losnummer" value="{{rand()}}"/>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <div class="form-row">
                    <h3 class="d-lg-none d-block sidetitle">Spielen und Gewinnen</h3>
                    <!--<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <fb:login-button
                                class="fb-login-button" data-size="large" 
                                data-width="100%"
                                data-button-type="continue_with"
                                scope="public_profile,email"
                                onlogin="checkLoginState();">
                        </fb:login-button>
                    </div>-->
                </div>
            </div>

            <div class="form-group form-check form-check-inline">
                <div class="form-row">
                    <div class="col-lg-12">
                        <input class="form-check-input" type="radio" name="anrede" id="herr" value="0">
                        <label class="form-check-label mr-3" for="herr">Herr</label>
                        <input class="form-check-input" type="radio" name="anrede" id="frau" value="1">
                        <label class="form-check-label" for="frau">Frau</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <input type="text" class="form-control" name="vorname" placeholder="Vorname"
                               onfocus="this.placeholder = ''" id="vorname">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <input type="text" class="form-control" name="nachname" placeholder="Nachname"
                               onfocus="this.placeholder = ''">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <div class="col-lg-12">
                        <input type="text" class="form-control" name="handy" placeholder="Handy"
                               onfocus="this.placeholder = ''">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <div class="col-lg-12">
                        <input type="text" class="form-control" name="email" placeholder="E-Mail"
                               onfocus="this.placeholder = ''">
                    </div>
                </div>
            </div>

            <div class="form-check hidemobile" style="font-size: 12px">
                <input class="form-check-input" type="checkbox" name="impressumform" id="impressumform">
                <label class="form-check-label" for="impressumform">
                    Ich bin damit einverstanden, dass die SP-Service Plattform GmbH und die im Impressum
                    genannten Sponsoren meine oben angegebenen Daten zu Werbe-, Marktforschungs- und
                    Analysezwecken erheben, verarbeiten und nutzen und dass ich von diesen per Post,
                    E-Mail
                    oder Telefon interessante Angebote, Service- und Produktinformationen über die in
                    der
                    Veranstalter-/Sponsorenliste aufgeführte Waren- und Dienstleistungen erhalte.Ich
                    kann
                    meine Einwilligung jederzeit mit Wirkung für die Zukunft kostenlos gegenüber der
                    SP-Service Plattform GmbH (E-Mail: widerruf@sp-plattform.de) und bezüglich der
                    Sponsoren
                    unter den jeweils angegebenen Kontaktdaten widerrufen.
                </label>
            </div>
            <div class="form-check hidemobile" style="font-size: 12px; margin-top: 10px;">
                <input class="form-check-input radio-primary" type="checkbox" name="datenschutzform"
                       id="datenschutzform">
                <label class="form-check-label" for="datenschutzform">
                    Ich habe die Hinweise zum <a href="https://www.sp-plattform.de/datenschutz.php" target="_blank">Datenschutz</a> zur Kenntnis genommen.
                </label>
            </div>

            <div class="form-group mt-4">
                <button type="submit" class="btn btn-block btn-danger" id="glucksrad-starten">
                    GLÜCKSRAD STARTEN
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    <p class="sidebarBtn">
        <span></span>
    </p>
</div>

