
<p>Liebe {{ $vorname }} {{ $nachname }},<br><br>
vielen Dank, dass Du am SP-Gewinnspiel teilgenommen hast!</p>

<p>Hier ist dein Gewinncode: <b>{{$losnummer}}</b>. Er ist 72 Stunden lang gültig.</p>

<p>Jetzt bist du nur noch einen Schritt von deinem Gewinn entfernt. Um mehr zu erfahren, rufst du am besten sofort an unter 0800 8882224 und lässt deinen Code bestätigen. <br>
Du erreichst uns werktags von 10:00 bis 18:00 Uhr. Dein Anruf ist selbstverständlich kostenlos.</p>

<p>Viel Glück!<br>
SP-Service Plattform GmbH<br><br>
Burak Tutan
</p>
<br>
<br>
<img src="{{ asset("images/mail-image.png")}}" height="130"/>
<br>
<br>
<p>SP-Service Plattform GmbH</p>

<p>
<b>Hauptsitz:</b><br>
Kornwestheimer Str. 179<br>
70825 Münchingen<br>
</p>
 
<p>
<b>Verwaltung:</b><br>
70825 Münchingen<br>
</p>
<p>
Mail: Burak.Tutan@sp-plattform.de
</p>
<p>
Geschäftsführung: Walter Kraus<br>
Gerichtsstand: Stuttgart, HRB 774562, USt-IdNr. DE261339313
</p>