@extends('admin.include.page.default')
@section('page')

    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><span class="font-weight-semibold">MeinProfil</span></h4>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-lg-12">

                {{ Form::open(array('method'=>'post', 'id' =>'ajax_profil_andern', 'enctype'=> 'multipart/form-data'  )) }}
                {{ csrf_field() }}
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-5">
                                <legend class="text-uppercase font-size-sm font-weight-bold">Ihre Daten</legend>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3">Vorname</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="vorname"
                                               value="{{$meinProfil[0]->vorname}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3">Nachname</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="nachname"
                                               value="{{$meinProfil[0]->nachname}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="text-right">
                            <button type="submit" class="btn btn-success legitRipple">
                                Speichern
                            </button>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">

                {{ Form::open(array('method'=>'post', 'id' =>'ajax_passwort_andern', 'enctype'=> 'multipart/form-data')) }}
                {{ csrf_field() }}
                <div class="card">

                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-5">

                                <legend class="text-uppercase font-size-sm font-weight-bold">Password Ändern</legend>
                                <div class="alert alert-info alert-styled-left alert-dismissible font-11">

                                    <h5>Ändern Sie Ihr Passwort von Zeit zu Zeit, um die Sicherheit zu erhöhen.</h5>
                                    <span class="font-weight-semibold">Unsere Empfehlungen für gute Passwörter</span>
                                    <br>
                                    <p><b>Anzahl der Zeichnen :</b>
                                        6 - 15 Zeichnen (davon mindestens 1 Zahl und 1 Buchstabe) <br>
                                        <b>Buchstaben :</b> a-z und A-Z, keine Umlaute <br>
                                        <b> Ziffern:</b> 0-9 <br>
                                        <b> Sonderzeichnen:</b> !'#*+?=§$%&/()=[]{}\;.,-</p>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3">Alte Passwort</label>
                                    <div class="col-lg-9">
                                        <input type="password" class="form-control" id="password_old"
                                               name="password_old">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3">Neue Passwort</label>
                                    <div class="col-lg-9 badge-indicator-absolute">
                                        <input type="password" class="form-control" id="password" name="password"
                                               minlength="6">
                                        <span class="badge password-indicator-badge-absolute"></span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3">Passwort Wiederholen</label>
                                    <div class="col-lg-9">
                                        <input type="password" class="form-control" id="password_re"
                                               name="password_re"
                                               minlength="6">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="text-right">
                            <button type="submit" class="btn btn-success legitRipple">
                                Speichern
                            </button>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('form#ajax_profil_andern')
                .formValidation({
                    message: 'This value is not valid',
                    feedbackIcons: {
                        valid: '  ',
                        invalid: ' ',
                        validating: ' '
                    },
                    fields: {
                        vorname: {
                            validators: {
                                notEmpty: {
                                    message: "<div class='text-danger pt-2'><i class='icon-cancel-circle2 pr-1'></i> Das Feld darf nicht leer sein</div>"
                                }
                            }
                        },
                        nachname: {
                            validators: {
                                notEmpty: {
                                    message: "<div class='text-danger pt-2'><i class='icon-cancel-circle2 pr-1'></i> Das Feld darf nicht leer sein</div>"
                                }
                            }
                        }
                    }
                })
                .on('success.form.fv', function (e) {
                    e.preventDefault();
                    var formdata = new FormData(this);
                    $.ajax({
                        url: '/ajax_profil_andern',
                        type: 'POST',
                        data: formdata,
                        processData: false,
                        contentType: false,
                        success: function (responce) {
                            if (responce) {
                                new PNotify({
                                    title: 'Erfolgreich',
                                    text: 'Ihre Profildaten wurde erfolgreich gespeichert',
                                    addclass: 'bg-success border-success',
                                    position: 'top-center'
                                });
                            } else {
                                new PNotify({
                                    title: 'Fehler!',
                                    text: 'Es ist ein Fehler aufgetreten. Bitte wenden Sie sich an Ihren Admin',
                                    addclass: 'bg-danger border-danger',
                                    position: 'top-center'
                                });
                            }
                        }
                    });
                });

            $('form#ajax_passwort_andern')
                .formValidation({
                    message: 'This value is not valid',
                    feedbackIcons: {
                        valid: '  ',
                        invalid: ' ',
                        validating: ' '
                    },
                    fields: {
                        password_old: {
                            validators: {
                                notEmpty: {
                                    message: "<div class='text-danger pt-2'><i class='icon-cancel-circle2 pr-1'></i>Bitte geben Sie sich alte Passwort ein</div>"
                                }
                            }
                        },
                        password: {
                            enabled: true,
                            validators: {
                                notEmpty: {
                                    message: "<div class='text-danger pt-2'><i class='icon-cancel-circle2 pr-1'></i>Bitte geben Sie sich neue Passwort ein</div>"
                                }
                            }
                        },
                        password_re: {
                            enabled: true,
                            validators: {
                                identical: {
                                    field: 'password',
                                    message: "<div class='text-danger pt-2'><i class='icon-cancel-circle2 pr-1'></i>Passwörter müssen übereinstimmen</div>"
                                }
                            }
                        }
                    }
                })

                .on('success.form.fv', function (e) {
                    // Prevent form submission
                    e.preventDefault();
                    var formdata_password = new FormData(this);
                    $.ajax({
                        url: '/ajax_passwort_andern',
                        type: 'POST',
                        data: formdata_password,
                        processData: false,
                        contentType: false,
                        success: function (responce) {
                            if (responce) {
                                new PNotify({
                                    title: 'Erfolgreich',
                                    text: 'Ihre Password wurde erfolgreich geändert',
                                    addclass: 'bg-success border-success',
                                    position: 'top-center'
                                });
                            } else {
                                new PNotify({
                                    title: 'Fehler',
                                    text: 'Ihre alte Passwort ist nicht korrekt. Bitte versuchen Sie sich wieder',
                                    addclass: 'bg-danger border-danger',
                                    position: 'top-center'
                                });

                            }
                        }

                    });
                });
        });
    </script>

@stop
