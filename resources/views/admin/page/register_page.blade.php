@extends('admin.include.login.default')
@section('login')
    <div class="page-content">
        <div class="content-wrapper">
            <div class="content d-flex justify-content-center align-items-center">
                <div class="card mb-0">
                    <div class="card-body">
                        <div class="text-center mb-3">
                            <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                            <h5 class="mb-0">REGISTRIEREN</h5>
                        </div>

                        <div class="login-form">
                            @if ($errors->count() > 0)
                                @foreach ($errors->all() as  $msg)
                                    <?php
                                    if ($errors->first('no') !== '' || $errors->first('email') !== '') {
                                        $error = " alert-danger";
                                    }
                                    if ($errors->first('yes') !== '') {
                                        $error = " alert-success";
                                    }
                                    ?>
                                @endforeach
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="alert <?= $error ?> alert-styled-left alert-dismissible">
                                            <span class="font-weight text-left">{{ $msg }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            {{ Form::open(array( 'class'=>'login-form', 'id'=>'login', 'role'=>'form', 'method'=>'post',
                                                        'url' =>URL::route('UserLoginProcess') )) }}
                            {{ csrf_field() }}
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="email" class="form-control" placeholder="E-Mail" name="email" id="email"
                                       required>
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                            </div>
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="password" class="form-control" name="password" minlength="6"
                                       placeholder="Password"
                                       required
                                       aria-required="true">
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <a href="login_password_recover.html" class="ml-auto">Passwort Vergessen?</a>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">ANMELDEN
                                    <i class="icon-circle-right2 ml-2"></i></button>
                            </div>

                            {{ Form::close() }}
                            <div class="form-group text-center text-muted content-divider">
                                <span class="px-2">or sign in with</span>
                            </div>
                            <div class="form-group">
                                <a href="/register" class="btn btn-light btn-block legitRipple">Registrieren</a>
                            </div>
                            <span class="form-text text-center text-muted">Powered By Eray Güclü Master of Engineering</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop