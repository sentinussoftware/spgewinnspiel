@extends('admin.include.page.default')
@section('page')
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><span class="font-weight-semibold">Spieler Details</span></h4>
            </div>
        </div>
    </div>
    <div class="content">
        {{ Form::open(array('method'=>'post', 'id' =>'ajax_spielerdata_save', 'enctype'=> 'multipart/form-data'  )) }}
        {{ csrf_field() }}
        <input type="hidden" name="losnummer" value="{{$spielerDetails[0]->losnummer}}"/>
        <input type="hidden" name="cookies" value="{{$spielerDetails[0]->cookies}}"/>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-5">
                                <fieldset class="mb-3">
                                    <legend class="text-uppercase font-size-sm font-weight-bold text-danger-600">Spieler
                                        Details
                                    </legend>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-3 font-weight-bold">Cookies</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" name="cookies"
                                                   value="{{$spielerDetails[0]->cookies}}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-3 font-weight-bold">Losnummer</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" name="losnummer"
                                                   value="{{$spielerDetails[0]->losnummer}}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-3 font-weight-bold">Status/Belohnung</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" name="status"
                                                   value="{{$spielerDetails[0]->status}}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-3 font-weight-bold">Anrede</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" name="anrede"
                                                   value="{{$spielerDetails[0]->anrede ==0 ? 'Herr': 'Frau'}}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-3 font-weight-bold">Vor- und
                                            Nachname</label>
                                        <div class="col-lg-9">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" name="vorname"
                                                           value="{{$spielerDetails[0]->vorname}}">
                                                </div>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" name="nachname"
                                                           value="{{$spielerDetails[0]->nachname}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>

                            </div>
                            <div class="col-lg-6 offset-1">
                                <fieldset class="mb-3">
                                    <legend class="text-uppercase font-size-sm font-weight-bold text-danger-600">
                                        Kontakt Details
                                    </legend>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-3 font-weight-bold">Strasse</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" name="strasse"
                                                   value="{{$spielerDetails[0]->strasse}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-3 font-weight-bold">PLZ / Ort</label>
                                        <div class="col-lg-9">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" name="plz"
                                                           value="{{$spielerDetails[0]->plz}}">
                                                </div>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" name="ort"
                                                           value="{{$spielerDetails[0]->ort}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-3 font-weight-bold">Handy</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" name="handy"
                                                   value="{{$spielerDetails[0]->handy}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-3 font-weight-bold">Fesnetz</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" name="festnetz"
                                                   value="{{$spielerDetails[0]->festnetz}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-3 font-weight-bold">E-Mail</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" name="email"
                                                   value="{{$spielerDetails[0]->email}}">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <fieldset class="mb-3">
                                    <legend class="text-uppercase font-size-sm font-weight-bold text-danger-600">
                                        Sonstige Notizen für die Kunde
                                    </legend>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-2">Notizen</label>
                                        <div class="col-lg-10">
                                            <textarea rows="3" cols="3" class="form-control" name="notizen"
                                                      placeholder="Bitte Schreiben sonstige Notizen">{{$spielerDetails[0]->notizen}}</textarea>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <button type="submit" class="btn btn-success legitRipple">
                                Speichern
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{ Form::close() }}
    </div>

    <script>
        $(document).ready(function () {
            $('form#ajax_spielerdata_save')
                .formValidation({
                    framework: 'bootstrap',
                    excluded: ':disabled',
                    fields: {
                        vorname: {
                            validators: {
                                notEmpty: {
                                    message: "<div class='text-danger font-size-sm'>Kann nicht leer sein</div>"
                                }
                            }
                        },
                        nachname: {
                            validators: {
                                notEmpty: {
                                    message: "<div class='text-danger font-size-sm'>Kann nicht leer sein</div>"
                                }
                            }
                        },

                        email: {
                            verbose: true,
                            validators: {
                                notEmpty: {
                                    message: "<div class='text-danger font-size-sm'>Kann nicht leer sein</div>"
                                },
                                emailAddress: {
                                    message: "<div class='text-danger font-size-sm'>Bitte geben ein E-Mail Adresse ein.</div>"
                                }
                            }
                        }
                    }
                })
                .on('success.form.fv', function (e) {
                    e.preventDefault();
                    var formdata = new FormData(this);
                    $.ajax({
                        url: '/ajax_spielerdata_save',
                        type: 'POST',
                        data: formdata,
                        processData: false,
                        contentType: false,
                        success: function (responce) {
                            if (responce) {
                                new PNotify({
                                    title: 'Erfolgreich',
                                    text: 'Ihre Profildaten wurde erfolgreich gespeichert',
                                    addclass: 'bg-success border-success',
                                    position: 'top-center'
                                });

                                setTimeout(
                                    function () {
                                        window.location = '/dashboard';
                                    }, 1000);
                            } else {
                                new PNotify({
                                    title: 'Fehler!',
                                    text: 'Es ist ein Fehler aufgetreten. Bitte wenden Sie sich an Ihren Admin',
                                    addclass: 'bg-danger border-danger',
                                    position: 'top-center'
                                });
                            }
                        }
                    });
                });

        });
    </script>



@stop
