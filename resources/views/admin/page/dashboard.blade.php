@extends('admin.include.page.default')
@section('page')




    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><span class="font-weight-semibold">SP SERVICEPLATFORM SPIELER </span></h4>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card pt-4">
                    <div class="pageloader">
                        <table id="spspielertable"
                               class="table table-hover display responsive no-wrap"
                               style="width:100%">
                            <thead>
                            <tr class="table-active">
                                <th>ID</th>
                                <th>Losnummer</th>
                                <th>Status/Belohnung</th>
                                <th>Name- und Vorname</th>
                                <th>E-Mail</th>
                                <th>Gespielt am</th>
                                <th>Aksion</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var ticketsdata;
        $(document).ready(function () {

            ticketsdata =  {!! json_encode($data) !!};
            console.log(ticketsdata)

            $('#spspielertable').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "responsive": true,
                "autoWidth": true,
                "destroy": true,
                "order": [[ 5, "desc" ]],
                "language": {
                    "url": "{{ asset("admin/js/datatables/language/German.json")}}",
                },
                "dom": 'Bfrtip',
                "data": ticketsdata,

                "buttons": [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "columns": [
                    {"data": "id"},
                    {"data": "losnummer"},
                    {"data": "status"},
                    {"data": "name"},
                    {"data": "email"},
                    {"data": "created_at"},
                    {"data": null}],
                "columnDefs": [
                    {
                        "targets": -1,
                        "orderable": false,
                        "render": function (data, type, row) {
                            $tablebutton = "<a href='/spielerdetails/" + data.losnummer + "' class='btn btn-link legitRipple mr-1'><i class='icon-database-edit2'></i></a>";
                            //$tablebutton += "<a href='#' class='btn btn-link legitRipple mr-1' disabled><i class='icon-trash'></i></a>";
                            return $tablebutton;
                        }
                    }]
            });

        });
    </script>
@stop
