<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark fixed-top bg-danger">
    <div class="navbar-brand">
        <a href="/dashboard" class="d-inline-block text-center">
            <img src="{{ asset('images/sp_logo.png') }}" alt="" height="55">
            <img src="{{ asset('images/sp_claim.png') }}" alt="">
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">

        <ul class="navbar-nav ml-md-auto">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle legitRipple"
                   data-toggle="dropdown" aria-expanded="false">
                    <span>{{\Illuminate\Support\Facades\Auth::user()->vorname}} {{\Illuminate\Support\Facades\Auth::user()->nachname}}</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="/meinprofil" class="dropdown-item"><i class="icon-user-plus"></i> Mein Profil</a>
                    <div class="dropdown-divider"></div>
                    <a href="/logout" class="dropdown-item"><i class="icon-switch2"></i> Abmelden</a>
                </div>
            </li>


        </ul>
    </div>
</div>
<!-- /main navbar -->