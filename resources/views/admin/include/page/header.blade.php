<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SP-GEWINSPIEL</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="{{ asset("admin/icons/icomoon/styles.min.css")}}" rel="stylesheet" type="text/css">
    <link href="{{ asset("admin/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css">
    <link href="{{ asset("admin/css/bootstrap_limitless.min.css")}}" rel="stylesheet" type="text/css">
    <link href="{{ asset("admin/css/layout.min.css")}}" rel="stylesheet" type="text/css">
    <link href="{{ asset("admin/css/components.min.css")}}" rel="stylesheet" type="text/css">
    <link href="{{ asset("admin/css/colors.min.css")}}" rel="stylesheet" type="text/css">

    <script src="{{ asset("admin/js/main/jquery.min.js")}}"></script>
    <script src="{{ asset("admin/js/main/bootstrap.bundle.min.js")}}"></script>
    <script src="{{ asset("admin/js/ui/ripple.min.js")}}"></script>
    <script src="{{ asset("admin/js/main/uniform.min.js")}}"></script>
    <script src="{{ asset("admin/js/datatables/datatables.min.js")}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script src="{{ asset("js/validation/formValidation.min.js")}}"></script>
    <script src="{{ asset("js/validation/formValidation-bootstrap.min.js")}}"></script>
    <script src="{{ asset("admin/js/notifications/pnotify.min.js")}}"></script>
    <script src="{{ asset("admin/js/notifications/noty.min.js")}}"></script>
    <script src="{{ asset("admin/js/form/form_controls_extended.js")}}"></script>
    <script src="{{ asset("admin/js/form/passy.js")}}"></script>

</head>
<body class="navbar-top">

