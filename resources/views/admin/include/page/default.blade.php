@include('admin.include.page.header')

<div class="page-content mt-2">
    @include('admin.include.page.navigation')
    @include('admin.include.page.sidebar')
    <div class="content-wrapper">
        @yield('page')
    </div>
</div>
@include('admin.include.page.footer')
