<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="{{ asset("admin/icons/icomoon/styles.min.css")}}" rel="stylesheet" type="text/css">

    <link href="{{ asset("admin/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css">
    <link href="{{ asset("admin/css/bootstrap_limitless.min.css")}}" rel="stylesheet" type="text/css">
    <link href="{{ asset("admin/css/layout.min.css")}}" rel="stylesheet" type="text/css">
    <link href="{{ asset("admin/css/components.min.css")}}" rel="stylesheet" type="text/css">
    <link href="{{ asset("admin/css/colors.min.css")}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="{{ asset("admin/js/main/jquery.min.js")}}"></script>
    <script src="{{ asset("admin/js/main/bootstrap.bundle.min.js")}}"></script>
    <script src="{{ asset("admin/js/plugins/loaders/blockui.min.js")}}"></script>
    <script src="{{ asset("admin/js/plugins/ui/ripple.min.js")}}"></script>


</head>

<body>